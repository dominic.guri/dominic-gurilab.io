---
title: Katex Rendering Test
subtitle: Math
date: 2018-06-07
tags: ["example", "photoswipe"]
categories: []
draft: true
---

$$
\int_B \psi(\theta) d\theta
$$


$$
\phi = \frac{(1+\sqrt{5})}{2} = 1.6180339887\cdots
$$
$$
\frac{1}{2}
$$

```julia
function predict(w,x0)
    x1 = pool(relu.(conv4(w[1],x0) .+ w[2]))
    x2 = pool(relu.(conv4(w[3],x1) .+ w[4]))
    x3 = relu.(w[5]*mat(x2) .+ w[6])
    return w[7]*x3 .+ w[8]
end
```
