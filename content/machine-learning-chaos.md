---
subtitle: "Predicting Chaos"
tags: []
---

__Based on the work by **[Edward Ott](http://www.chaos.umd.edu/personnel/ott.html)**__


Kuramoto-Sivashinsky Equation
-----------------------------
+ ___[Demo Code](https://github.com/E-Renshaw/kuramoto-sivashinsky)___ from the generosity of github user
+ In ___derivative form___:
    $$ u_t + u_{xxxx} + u_{xx} + uu_x =0, \quad x\in [-\frac{L}{2}, \frac{L}{2}]$$

+ In ___integral form___:
    $$ h_t + h_{xxxx} + h_{xx} + \frac{1}{2}h_x^2 = 0 $$

    + $u=h_x \sim$ attracts attention in complex spatio-temporal dynamics (***spatially-extended systems***)

