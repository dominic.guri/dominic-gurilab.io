---
<!--title: about me-->
<!--title: me-->
<!--subtitle: Why you'd want to hang out with me-->
comments: false
---

Welcome to my blog! My name is Dominic, and I am a Robotics PhD student at Carnergie Mellon University. 

The content on this blog is a conglomeration (mishmash, if you wish) of different topics and tools that I am learning. Just as an example, I have taken a liking to functional programming, and I am curious to see how [clash](https://clash-lang.org/) can help me use functional programming with FPGAs.

<!--I have recently become interested in functional programming.-->

<!--### my history-->
<!--will answer **all** your questions.-->
