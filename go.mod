module dominic.guri/dominic-guri.gitlab.io.git

go 1.16

require (
	github.com/EmielH/tale-hugo v2.2.0+incompatible // indirect
	github.com/jsnjack/kraiklyn v0.0.0-20210802120627-a2f56a7af778 // indirect
	github.com/kimcc/hugo-theme-noteworthy v0.0.0-20210708200524-5dcc67d31bf1 // indirect
	github.com/michimani/simplog v0.0.0-20210813031533-fdc9d9f2b996 // indirect
	github.com/mivinci/hugo-theme-minima v0.0.0-20210819044247-e7eb6932e25b // indirect
	github.com/natarajmb/charaka-hugo-theme v0.0.0-20201215113736-021bc743417b // indirect
	github.com/setsevireon/photophobia v0.0.6 // indirect
	github.com/shawnohare/hugo-tufte v0.0.0-20210131225155-e2af7d63ca1e // indirect
	github.com/wowchemy/starter-hugo-academic v0.0.0-20210815010200-73df027608ff // indirect
	github.com/zerostaticthemes/hugo-whisper-theme v0.0.0-20210709032611-82133a0311f4 // indirect
)
